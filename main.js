"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
// Модуль создающий CLI (Command Line Interface)
const readline = require("readline");
class App {
    constructor() {
        // State приложения
        this.state = 0;
        // True в рабочем цикле
        this.work = true;
        // Данные для даты
        this.y = 0;
        this.m = 0;
        this.d = 0;
    }
    // Собсна метод чтения строки с выводом сообщения
    readLine(message) {
        // Возвращаем обещание с типом результата "Строка"
        return new Promise((resolve) => {
            // Создаем интерфейс на стандартные потоки процесса
            let IF = readline.createInterface(process.stdin, process.stdout);
            // Делаем запрос с выводом форматированного сообщения
            IF.question('   -' + message + ' ', (s) => {
                // Шлем успешный результат в обещание
                resolve(s);
                // Закрываем интерфейс
                IF.close();
            });
        });
    }
    // Рабочая итерация программы
    Loop() {
        return __awaiter(this, void 0, void 0, function* () {
            // Переменная для введенных данных
            let data, date = new Date(this.y, this.m, this.d);
            switch (this.state) {
                case 0:
                    // Получаем строку
                    data = parseInt(yield this.readLine('Введите год:'));
                    // Проверяем формат
                    if (isNaN(data))
                        // Если все плохо говорим ай ай ай
                        return console.error('Год должен быть числом');
                    // Запоминаем данные
                    this.y = data;
                    // Поднимаем state
                    this.state++;
                    break;
                case 1:
                    // Получаем строку
                    data = parseInt(yield this.readLine('Введите месяц:'));
                    // Проверяем формат
                    if (isNaN(data) || data < 1 || data > 12)
                        // Если все плохо говорим ай ай ай
                        return console.error('Месяц должен быть числом не больше 12 и не меньше 1');
                    // Запоминаем данные
                    this.m = data - 1;
                    // Поднимаем state
                    this.state++;
                    break;
                case 2:
                    // Определяем максимальное количество дней в месяце
                    date.setMonth(date.getMonth() + 1);
                    date.setDate(0);
                    let maxDaysCount = date.getDate();
                    // Получаем строку
                    data = parseInt(yield this.readLine('Введите день:'));
                    // Проверяем формат
                    if (isNaN(data) || data < 1 || data > maxDaysCount)
                        // Если все плохо говорим ай ай ай
                        return console.error(`День должен быть числом не меньше 1 и не больше ${maxDaysCount}`);
                    // Запоминаем данные
                    this.d = data;
                    // Поднимаем state
                    this.state++;
                    break;
                case 3:
                    // Дата указанного дня
                    let dateSet = new Date(this.y, this.m, this.d);
                    // Дата первого дня указанного года
                    let dateNull = new Date(this.y, 0, 0);
                    // Разница в милисекундах
                    let diffranse = +dateSet - +dateNull;
                    // Количество милисекунд в одном дне
                    let oneDiffDay = 1000 * 60 * 60 * 24;
                    // Собсна делим разницу в милисекунд на количесво их в дне
                    let daysCount = Math.floor(diffranse / oneDiffDay);
                    // Выводим красиво
                    console.log('Порядковый номер указанного дня в году равен', daysCount);
                    // Поднимаем state
                    this.state++;
                    break;
                case 4:
                    // Получаем строку и смотрим результат
                    switch (yield this.readLine('Запустить повторно(y/n)?')) {
                        case 'y':
                            // если y то ставим state в начало
                            this.state = 0;
                            console.log('Повторный запуск...');
                            break;
                        case 'n':
                            // если n убираем true из рабочего цикла
                            this.work = false;
                            break;
                        default:
                            // если хз что то просим повторить
                            console.error('Команда не распознана!');
                    }
            }
        });
    }
    // Метод запуска программы
    Main() {
        return __awaiter(this, void 0, void 0, function* () {
            if (console.clear)
                console.clear();
            // Говорим дратути
            console.log('Добро пожаловать в программу расчета порядкового дня в году!');
            // Дрочим метод Loop пока есть true в свойстве work
            while (this.work)
                yield this.Loop();
            // Говорим дотвидания
            console.log('Спасибо за то, что воспользовались программой от WS-V');
        });
    }
}
(new App).Main();
