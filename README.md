# Зависимости

1. nodejs [Официальный сайт](https://nodejs.org/en/)
2. typescript [TypeScript(NPM)](https://www.npmjs.com/package/typescript)
3. VSCode [Официальный сайт](https://code.visualstudio.com/) (не обязательно)

# Описание

> Читаем все в main.ts

## Запуск:

### Желательно предварительно скомпилировать:

> tsc -p tsconfig.json

### Сам запуск

> node main.js